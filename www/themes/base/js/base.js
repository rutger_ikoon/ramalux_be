/**
 * @file
 * Placeholder file for custom sub-theme behaviors.
 *
 */
(function ($, Drupal) {

  /**
   * Use this behavior as a template for custom Javascript.
   */
  Drupal.behaviors.exampleBehavior = {
    attach: function (context, settings) {
      //alert("I'm alive!");
    }
  };

  /**
   * Use this behavior for viewport Animation
   */
  Drupal.behaviors.viewportAnimation = {
    attach: function (context, settings) {
      $(function(){
        AOS.init({
          once: true
        });
      });
    }
  };

  /**
   * Use this behavior as a template for custom Javascript.
   */
  Drupal.behaviors.scroll = {
    attach: function (context, settings) {
      $(document).ready(function(){
        $( "a.scroll-downs" ).click(function( event ) {
          event.preventDefault();
          $("html, body").animate({ scrollTop: $($(this).attr("href")).offset().top }, 500);
        });
      });

    }
  };

  /**
   * Use this behavior as a template for custom Javascript.
   */
  Drupal.behaviors.sliders = {
    attach: function (context, settings) {
      $(context).find('.field-name-field-slider.swiper-container').once('ifFieldSlider').each(function (){
        var fieldSlider = new Swiper ('.field-name-field-slider.swiper-container', {
          // Optional parameters
          loop: true,
          autoplay: {
            delay: 6000,
          },
          effect: 'fade',
          simulateTouch: false
        });
      });

      $(context).find('.reference.full .field-name-field-images.swiper-container').once('ifFieldSlider').each(function (){
        var referenceSlider = new Swiper('.reference.full .field-name-field-images.swiper-container', {
          spaceBetween: 30,
          slidesPerView: 'auto',
          autoHeight: 'auto',
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
        });
      });

      // $(context).find('.paragraph--type--carousel .field-name-field-images.swiper-container').once('ifFieldSlider').each(function () {
      //   var carouselSlider = new Swiper('.paragraph--type--carousel .field-name-field-images.swiper-container', {
      //     // Optional parameters
      //     loop: false,
      //     slidesPerView: 3,
      //     slidesPerColumn: 2,
      //
      //     slidesPerGroup: 3,
      //
      //     // Navigation arrows
      //     navigation: {
      //       nextEl: '.swiper-button-next',
      //       prevEl: '.swiper-button-prev',
      //     },
      //   });
      // });

      $(context).find('.paragraph--type--slider .field-name-field-images.swiper-container').once('ifFieldSlider').each(function () {
        var sliderSlider = new Swiper('.paragraph--type--slider .field-name-field-images.swiper-container', {
          // Optional parameters
          loop: false,

          // Navigation arrows
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
        });
      });
    }
  };

  /**
   * Use this behavior as a template for custom Javascript.
   */
  Drupal.behaviors.menu = {
    attach: function (context, settings) {
      $(context).find('#menuButton').once('ifMenu').each(function (){
        var menuButton = document.getElementById('menuButton');
        var nav = document.getElementById('navs');
        var header = document.getElementById('main-wrapper');

        menuButton.addEventListener('click', function (e) {
          menuButton.classList.toggle('is-active');
          if(menuButton.classList.contains('is-active')){
            nav.classList.add('open');
            nav.classList.remove('close');
            header.classList.add('drop-main-wrapper');
          }else {
            nav.classList.remove('open');
            nav.classList.add('close');
            header.classList.remove('drop-main-wrapper');
          }

          e.preventDefault();
        });
      });
    }
  };
  /**
     * Use this behavior as a template for custom Javascript.
     */
  Drupal.behaviors.form = {
    attach: function (context, settings) {

      $(context).find('#webform-submission-contact-node-11-add-form').once('ifFormItem').each(function () {
        $(document).ready(function () {
          resetForm();
        });
        function resetForm() {
          document.getElementById('webform-submission-contact-node-11-add-form').reset();
        }
        $('.form-item input, .form-item textarea').focusin(function () {
          $(this).parent().addClass('has-value');
        });

        $('.form-item input, .form-item textarea').blur(function () {
          if (!$(this).val().length > 0) {
            $(this).parent().removeClass('has-value');
          }
        });
      });
    }
  };




})(jQuery, Drupal);


